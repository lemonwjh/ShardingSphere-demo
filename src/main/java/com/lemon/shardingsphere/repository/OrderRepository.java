package com.lemon.shardingsphere.repository;

import com.lemon.shardingsphere.entity.Order;
import org.springframework.data.jpa.repository.JpaRepository;

public interface OrderRepository extends JpaRepository<Order, Long> {
}
